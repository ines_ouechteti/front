import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Acte } from './acte';

@Injectable({
  providedIn: 'root'
})
export class ActeService {

  apiurl: string;
  urlp:string;
  urp3 : string ;
  urp4 : string ;
  constructor(private http: HttpClient) { 
    this.apiurl='http://localhost:8484/webSERVICE/rest/affichage/';
    this.urlp='http://localhost:8484/webSERVICE/rest/acte/';
    this.urp3='http://localhost:8484/webSERVICE/rest/';
    this.urp4='http://localhost:8484/webSERVICE/rest/compte/';
    
  }
  getAllActe()
  {
    return this.http.get(this.apiurl + 'aff');
  }

  addACTE(l:Acte):Observable<Object>
  {
    return this.http.post(this.urlp + 'A', l);
  }
  supprimerActe(reference : string): Observable<any>
  {
    return this.http.delete(this.urp3 + 'acte/' + reference , { responseType: 'text' });
  }
  updateActe(l:Acte): any
  {
    return this.http.put(this.urp4+ 'U', l);
  }
  getActeByreference(reference:string):Observable<any>
  {
      return this.http.get(this.apiurl +'/' +reference);
  }


}
