import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/user.service';
import { Utilisateur } from 'src/app/utilisateur';

@Component({
  selector: 'app-registre',
  templateUrl: './registre.component.html',
  styleUrls: ['./registre.component.css']
})
export class RegistreComponent implements OnInit {
  l: Utilisateur = new Utilisateur();
  constructor(private UserService: UserService,
               private router: Router ) {
              
   }
   ngOnInit(): void {

   }
 
   addlivre(): void
   {
     this.UserService.addAdmin(this.l)
       .subscribe(data=>
       {
         console.log(data);
       }, error => console.log(error));
   }


   toActe(): void
   {
       this.router.navigate(['/login']);
   }



   onSubmit():void
{
this.UserService.addAdmin(this.l).subscribe(
(data)=>console.log(data),

(error)=>console.log(error),
()=>this.toActe()

);
console.log(this.l);

}

   retour(): void {
     this.router.navigate(['/login']);
   }


}