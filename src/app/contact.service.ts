import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Contact } from './contact';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  apiurl:string ;
  constructor(private http: HttpClient) { 
    this.apiurl='http://localhost:8484/webSERVICE/rest/compte/'; }
    
  addContact(l:Contact):Observable<Object>
  {
    return this.http.post(this.apiurl + 'A', l);
  }
}
