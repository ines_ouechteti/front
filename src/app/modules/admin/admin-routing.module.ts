import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { ActesComponent } from './components/actes/actes.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { ContactComponent } from './components/contact/contact.component';
import { DonnationComponent } from './components/donnation/donnation.component';
import { HomeComponent } from './components/home/home.component';
import { ProfilComponent } from './components/profil/profil.component';
import { ServicesComponent } from './components/services/services.component';
import { UpdateActeComponent } from './components/update-acte/update-acte.component';

const routes: Routes = [
  {path:'',component:AdminDashboardComponent,
  children:[
    {path:'home',component:HomeComponent},
    {path:'about',component:AboutComponent},
    {path:'services',component:ServicesComponent},
    {path:'contact',component:ContactComponent},
    {path:'donation',component:DonnationComponent},
    {path:'actes',component:ActesComponent},
    {path:'profil',component:ProfilComponent},
    {path:'update/:reference',component:UpdateActeComponent},
    {path:'',redirectTo:'/admin/home',pathMatch:'full'},
  ],
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
