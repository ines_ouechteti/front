import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Acte } from 'src/app/acte';
import { ActeService } from 'src/app/acte.service';

@Component({
  selector: 'app-actes',
  templateUrl: './actes.component.html',
  styleUrls: ['./actes.component.css']
})
export class ActesComponent implements OnInit {

 
  l: Acte = new Acte();
  constructor(private ActeService: ActeService,
               private router: Router) {
   }
   ngOnInit(): void {

   }
   addActe(): void
   {
     this.ActeService.addACTE(this.l)
       .subscribe(data=>
       {
         console.log(data);
       }, error => console.log(error));
   }


   toActe(): void
   {
       this.router.navigate(['/admin/donation']);
   }



   onSubmit():void
{
this.ActeService.addACTE(this.l).subscribe(
(data)=>console.log(data),

(error)=>console.log(error),
()=>this.toActe()

);
console.log(this.l);

}

   retour(): void {
     this.router.navigate(['/admin/donation']);
   }

}
