import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Contact } from 'src/app/contact';
import { ContactService } from 'src/app/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  l: Contact = new Contact();
  constructor(private ContactService: ContactService,
               private router: Router) {
   }
   ngOnInit(): void {

   }
   addlivre(): void
   {
     this.ContactService.addContact(this.l)
       .subscribe(data=>
       {
         console.log(data);
       }, error => console.log(error));
   }


   toActe(): void
   {
       this.router.navigate(['/donation']);
   }



   onSubmit():void
{
this.ContactService.addContact(this.l).subscribe(
(data)=>console.log(data),

(error)=>console.log(error),
()=>this.toActe()

);
console.log(this.l);

}

   retour(): void {
     this.router.navigate(['/donation']);
   }


}
