import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Acte } from 'src/app/acte';
import { ActeService } from 'src/app/acte.service';



@Component({
  selector: 'app-donnation',
  templateUrl: './donnation.component.html',
  styleUrls: ['./donnation.component.css']
})
export class DonnationComponent implements OnInit {
  actes : Acte[] =new Array;
 
    constructor(private ActeService: ActeService,
      private router: Router ){
   
    }

  ngOnInit():void {
    this.reloadData();
  }
 
  reloadData():void{

    this.ActeService.getAllActe().subscribe(data=>{
        this.actes=data as Acte [];
    });
}
  
deleteActe(reference: any)
    {
        this.ActeService.supprimerActe(reference).subscribe(data=>
          {
              console.log(data);
              this.reloadData();
          },
            error=>console.log(error));
          }

          toUpdate(reference:any){
            this.router.navigate(['/admin/update',reference]);
        }


}
