import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/user.service';
import { Utilisateur } from 'src/app/utilisateur';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {


  l: Utilisateur = new Utilisateur();
  cin:any;


    constructor(private service :UserService,
                private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit(): void {
      this.cin = this.route.snapshot.params. cin;
      this.service.getAdminByreference(this.cin)
        .subscribe(  data => {
          console.log(data);
          this.l = data;
        }, error => console.log(error));
    }

    retour(): void {
      this.router.navigate(['/admin/donation']);
    }
    toDonation(): void
    {
      this.router.navigate(['/admin/donation']);

    }
    onSubmit(): void {
      this.service.updateAdmin(this.l)
        .subscribe((data: any) =>
        console.log(data), (error: any) => console.log(error));
      this.toDonation();
     
    }


}
