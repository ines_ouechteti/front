import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateActeComponent } from './update-acte.component';

describe('UpdateActeComponent', () => {
  let component: UpdateActeComponent;
  let fixture: ComponentFixture<UpdateActeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateActeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateActeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
