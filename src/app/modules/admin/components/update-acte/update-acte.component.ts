import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Acte } from 'src/app/acte';
import { ActeService } from 'src/app/acte.service';

@Component({
  selector: 'app-update-acte',
  templateUrl: './update-acte.component.html',
  styleUrls: ['./update-acte.component.css']
})
export class UpdateActeComponent implements OnInit {

  l: Acte = new Acte();
    reference:any;


      constructor(private service :ActeService,
                  private router: Router,
                  private route: ActivatedRoute) {
      }

      ngOnInit(): void {
        this.reference = this.route.snapshot.params. reference;
        this.service.getActeByreference(this.reference)
          .subscribe(  data => {
            console.log(data);
            this.l = data;
          }, error => console.log(error));
      }

      retour(): void {
        this.router.navigate(['/admin/donation']);
      }
      toDonation(): void
      {
        this.router.navigate(['/admin/donation']);

      }
      onSubmit(): void {
        this.service.updateActe(this.l)
          .subscribe((data: any) =>
          console.log(data), (error: any) => console.log(error));
        this.toDonation();
        this.service.getAllActe();
      }

}
