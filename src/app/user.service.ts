import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Utilisateur } from './utilisateur';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiurl:string ;
  constructor(private http: HttpClient) { 
    this.apiurl='http://localhost:8484/webSERVICE/rest/compte/'; }
    
  addAdmin(l:Utilisateur):Observable<Object>
  {
    return this.http.post(this.apiurl + 'C', l);
  }
  updateAdmin(l:Utilisateur): any
  {
    return this.http.put(this.apiurl+ 'Update', l);
  }
  getAdminByreference(cin:number):Observable<any>
  {
      return this.http.get(this.apiurl +'' +cin);
  }
}
